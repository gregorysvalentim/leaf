﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrDeadZone : MonoBehaviour
{
    [SerializeField] GameObject PainelLoad;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            PainelLoad.SetActive(true);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
