﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrMudaCena : MonoBehaviour
{
    [SerializeField] int cena;
    [SerializeField] GameObject PainelLoad;
    public void Troca()
    {
        switch(cena)
        {
            case 1:
                {
                    GameObject.Find("Save").GetComponent<ScrSave>().Fase1Passada = true;
                    break;
                }
                case 2:
                {
                    Cursor.visible = true;
                    Destroy(GameObject.Find("Save"));
                    break;
                }
        }
        PainelLoad.SetActive(true);
        SceneManager.LoadScene(cena);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Troca();
        }
    }
}
