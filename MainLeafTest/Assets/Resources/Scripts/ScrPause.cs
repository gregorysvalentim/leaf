﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrPause : MonoBehaviour
{
    [SerializeField] GameObject MenuPanel;

    private void Start()
    {
        if (GameObject.Find("Save").GetComponent<ScrSave>().Fase1Passada == true && SceneManager.GetActiveScene().buildIndex == 0)
        {
            GameObject.Find("Caixa").GetComponent<Transform>().transform.position = new Vector3(-9.7f, 2.1f, -0.4f);
            GameObject.Find("Caixa (1)").GetComponent<Transform>().transform.position = new Vector3(-9.7f, 5.1f, -0.4f);
        }
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            MenuPanel.SetActive(true);
            Time.timeScale = 0;
            Cursor.visible = true;
        }
    }
    public void Resumir()
    {
        MenuPanel.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
    }
    public void Sair()
    {
        Application.Quit();
    }
}
