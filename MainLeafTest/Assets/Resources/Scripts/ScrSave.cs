﻿using UnityEngine;

public class ScrSave : MonoBehaviour
{
    public bool Fase1Passada = false;
    public static ScrSave control;
    void Start()
    {
        if (control == null)
        {
            DontDestroyOnLoad(gameObject);
            control = this;
        }
        else
        {
            if (control != this)
            {
                Destroy(gameObject);
            }
        }
    }

}
