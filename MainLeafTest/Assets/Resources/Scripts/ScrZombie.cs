﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ScrZombie : MonoBehaviour
{
    //Movimentação
    [SerializeField] Vector3[] Posicoes;
    [SerializeField] int[] EixoRotacao;
    int indexPos = 0;
    bool Movimento = false;

    //Visão
    private bool isInFov = false;
    public float maxAngle;
    public float maxRadius;
    public LayerMask Layer;
    public Transform Player;


    bool derrota = false;
    [SerializeField] GameObject PainelLoad;
    private void Start()
    {
        StartCoroutine(Move());
    }

    private void Update()
    {
        isInFov = inFOV(transform, Player, maxAngle, maxRadius, Layer,derrota,PainelLoad);

        if (!isInFov)
        {
            Gizmos.color = Color.red;
        }
        else
        {
            derrota = true;
        }
        if (Movimento == true)
        {
            if (transform.eulerAngles.y < EixoRotacao[indexPos] - 5 || transform.eulerAngles.y > EixoRotacao[indexPos] + 5)
            {
                this.gameObject.transform.Rotate(new Vector3(0, 1, 0), 1 * 5f);
            }
            else
            {
                if (transform.position != Posicoes[indexPos])
                {
                    transform.position = Vector3.MoveTowards(transform.position, Posicoes[indexPos], 0.05f);
                    this.GetComponent<Animator>().Play("Walk");
                }
                else
                {
                    this.GetComponent<Animator>().Play("Idle");
                }
            }
        }
        
    }

    IEnumerator Move()
    {
        if(transform.position == Posicoes[indexPos])
        {
            indexPos++;
            if(indexPos == 4)
            {
                indexPos = 0;
            }
            Movimento = false;
        }
        else
        {
            Movimento = true;
        }
        yield return new WaitForSeconds(5);

        StartCoroutine(Move());
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, maxRadius);
        Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.up) * (transform.forward * -1) * maxRadius;
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.up) * (transform.forward * -1) * maxRadius;
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, fovLine1);
        Gizmos.DrawRay(transform.position, fovLine2);
        Gizmos.DrawRay(transform.position, (Player.position - transform.position).normalized * maxRadius);
        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);
    }
    
    public static bool inFOV(Transform checkingObject, Transform target, float maxAngle, float maxRadius, LayerMask Layer, bool derrota, GameObject PainelLoad)
    {
        Collider[] overlaps = new Collider[100];
        int count = Physics.OverlapSphereNonAlloc(checkingObject.position, maxRadius, overlaps);
        for (int i = 0; i < count + 1; i++)
        {
            if (overlaps[i] != null)
            {
                if (overlaps[i].transform == target)
                {
                    Vector3 directionBetween = (target.position - checkingObject.position).normalized;
                    directionBetween.y *= 0;

                    float angle = Vector3.Angle(checkingObject.forward * -1, directionBetween);

                    if (angle <= maxAngle)
                    {
                        Ray ray = new Ray(checkingObject.position, target.position - checkingObject.position);
                        RaycastHit hit;

                        if (Physics.Raycast(ray, out hit, maxRadius, Layer))
                        {
                            if (hit.transform == target)
                            {
                                if(derrota == false)
                                {
                                    PainelLoad.SetActive(true);
                                    SceneManager.LoadScene(0);
                                }
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
}
