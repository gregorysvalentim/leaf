﻿using UnityEngine;

public class ScrMoeda : MonoBehaviour
{
    public int Valor;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<ScrPlayer1>().AddMoeda(Valor);
            Destroy(this.gameObject);
        }
    }
}
