﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class ScrPlayer1 : MonoBehaviour
{
    //Componentes
    Rigidbody Corpo;
    Animator Anim;
    //Variaveis Customizadas
    [SerializeField]
    private float Velocidade = 4f;
    [SerializeField]
    private float ForcaPulo = 4f;
    //Controladores
    float EixoX;
    float EixoZ;
    float EixoRotacao = 0;
    int DirRotacao = 0;
    bool Agachado = false;

    //Objetos
    GameObject objSegurado;

    //Valores
    [SerializeField] int Moedas = 0;

    //Colliders
    BoxCollider Box;
    CapsuleCollider Capsule;

    GameObject PegarTexto;
    void Start()
    {
        Corpo = this.GetComponent<Rigidbody>();
        Anim = this.GetComponent<Animator>();
        Box = this.GetComponent<BoxCollider>();
        Capsule = this.GetComponent<CapsuleCollider>();
        PegarTexto = GameObject.Find("Txt_Pegar");
        PegarTexto.SetActive(false);
        Cursor.visible = false;
    }

    
    void Update()
    {
        EixoX = Input.GetAxisRaw("Horizontal") * Velocidade * Time.deltaTime;
        EixoZ = Input.GetAxisRaw("Vertical") * Velocidade * Time.deltaTime;
        if(SceneManager.GetActiveScene().buildIndex == 0)
        {
            transform.eulerAngles = new Vector3(0, GameObject.Find("CM vcam1").transform.rotation.eulerAngles.y, 0);
        }
        else
        {
            Rotacionar();
        }

        if(Input.GetKey(KeyCode.LeftControl))
        {
            Agachar(true);
        }
        else
        {
            Agachar(false);
        }

        if (Agachado == false)
        {
            VerificaChao();
            VerificaFrente();
        }
    }
    
    void FixedUpdate()
    {
            if (EixoX != 0 || EixoZ != 0)
            {
                
                if (objSegurado != null && Agachado == false)
                {
                
                    if (SceneManager.GetActiveScene().buildIndex == 0)
                    {
                        transform.Translate(new Vector3(EixoX / 1.5f, 0, EixoZ / 1.5f), Space.Self);
                    }
                    else
                    {
                        Corpo.velocity = new Vector3(EixoX * 50, Corpo.velocity.y, EixoZ * 50);
                    }
                    Anim.Play("WalkGrab");
                    MovimentarObj();
                    
                }
                else
                {
                    if (SceneManager.GetActiveScene().buildIndex == 0)
                    {
                        transform.Translate(new Vector3(EixoX , 0, EixoZ), Space.Self);
                    }
                    else
                    {
                        Corpo.velocity = new Vector3(EixoX *50, Corpo.velocity.y, EixoZ * 50);
                    }
                    if (Agachado == false)
                    {
                        Anim.Play("Walk");
                    }
                }
            }
            else
            {
                Corpo.velocity = new Vector3(0,Corpo.velocity.y,0);
                if (objSegurado != null)
                {
                    objSegurado.GetComponent<Rigidbody>().velocity = new Vector3(0, objSegurado.GetComponent<Rigidbody>().velocity.y, 0);
                }
                else
                {
                    if (Agachado == false)
                    {
                        Anim.Play("Idle");
                    }
                
                }

            }
        
    }
    void MovimentarObj()
    {
        if (transform.eulerAngles.y < 45 && transform.eulerAngles.y > 0 || transform.eulerAngles.y > 330)
        {
            objSegurado.GetComponent<Rigidbody>().velocity = new Vector3(EixoX * Velocidade, objSegurado.GetComponent<Rigidbody>().velocity.y, EixoZ * Velocidade);

        }
        else
        {
            if (transform.eulerAngles.y < 215 && transform.eulerAngles.y > 140)
            {
                objSegurado.GetComponent<Rigidbody>().velocity = new Vector3((EixoX * -1) * Velocidade, objSegurado.GetComponent<Rigidbody>().velocity.y, (EixoZ * -1) * Velocidade);
            }
            else
            {
                if (transform.eulerAngles.y < 135 && transform.eulerAngles.y > 50)
                {
                    objSegurado.GetComponent<Rigidbody>().velocity = new Vector3((EixoZ) * Velocidade, objSegurado.GetComponent<Rigidbody>().velocity.y, (EixoX * -1) * Velocidade);

                }
                else
                {
                    objSegurado.GetComponent<Rigidbody>().velocity = new Vector3((EixoZ * -1) * Velocidade, objSegurado.GetComponent<Rigidbody>().velocity.y, (EixoX) * Velocidade);
                }
            }
        }
    }
    void VerificaFrente()//Verifica se a caixa esta no alcance
    {
        RaycastHit hitD;
        Physics.Raycast(transform.position + transform.TransformDirection(Vector3.forward), transform.TransformDirection(Vector3.forward), out hitD, 2f);
        if (hitD.collider != null && hitD.collider.gameObject != this.gameObject && hitD.collider.gameObject.tag == "Caixa")
        {
            PegarTexto.SetActive(true);
            if (Input.GetMouseButtonDown(0))
            {
                if (objSegurado == null)
                {
                    objSegurado = hitD.collider.gameObject;
                }
                else
                {
                    objSegurado.GetComponent<Rigidbody>().velocity = new Vector3(0, objSegurado.GetComponent<Rigidbody>().velocity.y, 0);
                    objSegurado = null;
                }
            }
        }
        else
        {
            PegarTexto.SetActive(false);
            if (objSegurado != null)
            {
                objSegurado.GetComponent<Rigidbody>().velocity = new Vector3(0, objSegurado.GetComponent<Rigidbody>().velocity.y, 0);
            }

            objSegurado = null;
        }
    }
    void VerificaChao()//Verifica se esta no chao
    {
        RaycastHit hitChao;
        Physics.Raycast(transform.position + transform.TransformDirection(Vector3.down), transform.TransformDirection(Vector3.down), out hitChao, 2f);
        if (hitChao.collider != null && hitChao.collider.gameObject != this.gameObject)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Pular();
            }
        }
    }
    void Pular()
    {
        Corpo.AddForce(Vector3.up * (ForcaPulo), ForceMode.Impulse);
        Anim.Play("Jump");
    }
    void Agachar(bool Escolha)
    {
        switch (Escolha)
        {
            case true:
                {
                    Anim.Play("Crouch");
                    Box.enabled = true;
                    Capsule.enabled = false;
                    Agachado = true;
                    break;
                }
            case false:
                {
                    Box.enabled = false;
                    Capsule.enabled = true;
                    Agachado = false;
                    break;
                }
        }
    }
    public void AddMoeda(int Valor)
    {
        Moedas += Valor;
        GameObject.Find("Txt_Moedas").GetComponent<TMP_Text>().text = Moedas.ToString("00");
    }
    void VerificaDirRotacao(int num)
    {
        if (EixoRotacao != num)
        {
            if (num - transform.eulerAngles.y > 20 && num - transform.eulerAngles.y < 180 || (num - transform.eulerAngles.y) * -1 > 200 || (num - transform.eulerAngles.y) * -1 > 50 && (num - transform.eulerAngles.y) * -1 < 52)
            {
                DirRotacao = 1;
            }
            else
            {
                DirRotacao = -1;
            }
        }
    }

    void Rotacionar()
    {
        if (EixoZ > 0)
        {
            if (EixoX == 0)
            {
                VerificaDirRotacao(0);
                EixoRotacao = 0;

            }
            else
            {
                if (EixoX > 0)
                {
                    VerificaDirRotacao(45);
                    EixoRotacao = 45;
                }
                else
                {
                    if (EixoX < 0)
                    {
                        VerificaDirRotacao(300);
                        EixoRotacao = 300;
                    }
                }
            }
        }
        else
        {
            if (EixoZ < 0)
            {
                if (EixoX == 0)
                {
                    VerificaDirRotacao(180);
                    EixoRotacao = 180;
                }
                else
                {
                    if (EixoX > 0)
                    {
                        VerificaDirRotacao(160);
                        EixoRotacao = 160;
                    }
                    else
                    {
                        if (EixoX < 0)
                        {
                            VerificaDirRotacao(225);
                            EixoRotacao = 225;
                        }
                    }
                }
            }
            else
            {
                if (EixoZ == 0)
                {
                    if (EixoX > 0)
                    {
                        VerificaDirRotacao(90);
                        EixoRotacao = 90;
                    }
                    else
                    {
                        if (EixoX < 0)
                        {
                            VerificaDirRotacao(270);
                            EixoRotacao = 270;
                        }
                    }

                }
            }
        }
        if (transform.eulerAngles.y < EixoRotacao - 5 || transform.eulerAngles.y > EixoRotacao + 5 && objSegurado == null)
        {
            this.gameObject.transform.Rotate(new Vector3(0, 1, 0), DirRotacao * 10f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PortaAgachar")
        {
            PegarTexto.GetComponent<TMP_Text>().text = "AGACHAR";
            PegarTexto.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PortaAgachar")
        {
            PegarTexto.GetComponent<TMP_Text>().text = "PEGAR";
            PegarTexto.SetActive(false);
        }
    }
}
